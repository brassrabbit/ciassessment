package calculatorPackage;

public class Calculator {
	
	public int addition(int a, int b) {

		int result = a + b;

		return result;
	}
	
	public int subtract(int a, int b) {

		int result = a - b;

		return result;
	}
	
	public int multiply(int a, int b) {

		int result = a * b;

		return result;

	}
	
	public float divide(int a, int b) {

		float result = (float) a / b;

		return result;

	}
}
