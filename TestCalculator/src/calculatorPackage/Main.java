package calculatorPackage;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Calculator calculator = new Calculator();

		int a, b;
		String operation, answer;

		do {

			System.out.println("Please enter first number: ");

			a = input.nextInt();

			System.out.println("Please enter second number: ");

			b = input.nextInt();

			System.out.println("Please enter operation (add/subtract/multiply/divide) :");

			operation = input.next();

			switch (operation) {

			case "add": {
				System.out.println("When you " + operation + " " + a + " and " + b + ", the result is: "
						+ calculator.addition(a, b));
				break;

			}
			case "subtract": {
				System.out.println("When you " + operation + " " + a + " and " + b + ", the result is: "
						+ calculator.subtract(a, b));
				break;
			}
			case "multiply": {
				System.out.println("When you " + operation + " " + a + " and " + b + ", the result is: "
						+ calculator.multiply(a, b));
				break;
			}
			case "divide": {
				System.out.println("When you " + operation + " " + a + " and " + b + ", the result is: "
						+ calculator.divide(a, b));
				break;
			}
			default: {
				System.out.println("Incorrect input, please try again");
				break;
			}
			}
			System.out.println("Would you like to continue? (y/n)");
			
			answer = input.next();
			
		} while (answer == "y");
		
		input.close();
	}

}
